import styled from 'styled-components'
import React from 'react'
import { Profile } from './user-profile'
import { Button } from 'antd';
import config from 'config'
import { useHistory } from "react-router-dom";



const HeaderWrapper = styled.div`
position: absolute;
width: 1440px;
height: 64px;
left: 0px;
top: 0px;

background: #FFFFFF;
box-shadow: 0px 4px 10px rgba(219, 219, 219, 0.25);

display:flex;
justify-content: space-between;

img{
  margin-left:16px;
}

.user-profile{
  /* margin-right:14px; */
}
`

const LeftWrapper = styled.div`
display:flex;
.menu-list{
  display:flex;
  width: 30em;
  justify-content: space-evenly;
  align-items: center;
  .menu-text{
    /* Body Medium */
    font-family: Inter;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    /* identical to box height, or 143% */
    display: flex;
    align-items: center;
    color: #000000;
    }
  .menu-text-actived{
    /* H200 */

    font-family: Inter;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    /* identical to box height, or 143% */

    display: flex;
    align-items: center;

    /* Primary */

    color: #2BA6E3;
  }
}


`

const Loginout = ({ url }) => {
  let history = useHistory();
  // return <Button onClick={() => window.location.href = `http://report.ilotusland.localhost:${config.serverPort}/login`} type="primary">Login</Button>
  return <Button onClick={() => history.push('/login')} type="primary">Login</Button>
}
const MenuList = () => {
  return (
    <div className='menu-list'>
      <div className='menu-text-actived'>List</div>
      <div className='menu-text'>Settings</div>
      <div className='menu-text'>Sharing</div>
    </div>
  )
}
const Header = ({ logo, avatar, fullName, url }) => {
  return <HeaderWrapper>
    <LeftWrapper>
      <img src={logo} alt='app logo' />
      <MenuList />
    </LeftWrapper>

    {
      avatar && fullName ?
        <Profile className='user-profile' avatar={avatar} fullName={fullName} /> :
        <Loginout className='user-profile' />
    }
  </HeaderWrapper>
}

export { Header }

