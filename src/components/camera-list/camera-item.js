import React from 'react'
import styled from 'styled-components'

const CameraItemWrapper = styled.div`
margin:2em;
div{
  margin-bottom:4px;
  font-size: medium;
  font-weight: bold;
}
`

const CameraItem = ({ name, thumbnail }) => {
  return <CameraItemWrapper>
    <div>{name}</div>
    <img src={thumbnail} alt='cam thumbnail' />
  </CameraItemWrapper>
}

export { CameraItem }