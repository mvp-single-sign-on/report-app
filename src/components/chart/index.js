import React from 'react'
// import { withWidget } from '../widget'
import Widget from '../widget'
import styled from "styled-components";
import dayjs from "dayjs";
import { ResponsiveBar } from "@nivo/bar";



export const ReportDetailContainer = styled.div`
 flex: 1;
 height: 100vh;
 width: 100vw;
 padding: 16px;
`;
export const GroupContainer = styled.div`
 background-color: white;
 padding: 8px;
 border-radius: 10px;
`;
export const HeaderGroup = styled.div`
 display: flex;
 flex-direction: row;
 justify-content: space-between;
 padding: 0 8px;
 align-items: center;
`;
export const HeaderTitle = styled.span`
 font-size: 16px;
 line-height: 24px;
`;
export const HeaderSubTitle = styled.span`
 color: #a1a1a1;
`;
export const HeaderRight = styled.div`
 font-weight: 600;
 font-size: 12px;
 line-height: 16px;
 color: #656565;
`;
export const BodyDashboard = styled.div`
 display: flex;
 align-items: center;
 justify-content: space-between;
 margin-top: 16px;
`;
export const DashboardItem = styled.div`
 display: flex;
 flex-direction: column;
 align-items: center;
 justify-content: center;
 padding-right: 8px;
`;
export const DashboardItemTitle = styled.p`
 font-weight: bold;
 font-size: 14px;
 line-height: 20px;
 margin-bottom: 8px;
`;
export const DashboardItemCount = styled.p`
 font-weight: 900;
 font-size: 24px;
 line-height: 36px;
`;
export const DashboardList = styled.div`
 display: flex;
 align-items: center;
 justify-content: center;
 margin-left: 8px;
`;


const Chart = ({ data }) => {
  // console.log('==Chart====')
  // return
  // <Widget>
  return (
    <GroupContainer style={{ marginTop: "16px", width: '30%', margin: '2em' }}>
      <HeaderGroup>
        <HeaderTitle className="text-bold">
          Today
  <HeaderSubTitle className="text-bold">&nbsp;stations</HeaderSubTitle>
        </HeaderTitle>
        <HeaderRight>{dayjs().format("HH:MM")}</HeaderRight>
      </HeaderGroup>
      <div style={{ height: "300px", width: "100%" }}>
        <ResponsiveBar
          data={data}
          keys={["station"]}
          indexBy="country"
          margin={{ top: 50, right: 0, bottom: 50, left: 30 }}
          padding={0.4}
          borderRadius={4}
          enableGridY={false}
          enableLabel={false}
          colors={(d) => d.data.burgerColor}
          animate={true}
          motionStiffness={90}
          motionDamping={15}
        />
      </div>
    </GroupContainer>
  )
  // </Widget>
}
export { Chart }
// export { Widget(Chart) }
// export default withWidget(Chart)