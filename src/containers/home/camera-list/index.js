import React from 'react'
import styled from 'styled-components'
import { CameraList } from 'components/camera-list'

const CameraWrapper = styled.div`
margin-left:151px;
margin-right:151px;
margin-top:136px;
padding:32px;
background:red;
width:100%;
height:100%;
background:#FAFBFB;
display: flex;
flex-wrap: wrap;
`


const data = [
  {
    name: 'Hang Dau',
    thumbnail: '/images/sample-cameras/cam1.svg'
  },
  {
    name: 'Hoan Kiem',
    thumbnail: '/images/sample-cameras/cam2.svg'
  },
  {
    name: 'Minh Khai',
    thumbnail: '/images/sample-cameras/cam3.svg'
  },
  {
    name: 'Vinh Tan',
    thumbnail: '/images/sample-cameras/cam4.svg'
  }
]

const CameraListContainer = () => {
  return <CameraWrapper><CameraList data={data} /></CameraWrapper>
}

export { CameraListContainer }