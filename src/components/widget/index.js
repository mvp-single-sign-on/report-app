import React from 'react'
import styled from 'styled-components'

const WidgetWrapper = styled.div`
background: #FFFFFF;
/* Small Shadow */

box-shadow: 0px 4px 8px rgba(26, 26, 26, 0.2);
border-radius: 10px;
`

const Widget = ({ children }) => {
  return <WidgetWrapper>{children}</WidgetWrapper>
}

export default Widget

// const withWidget = WrappedComponent => {
//   console.log('==withWidget===')
//   return <WidgetWrapper>
//     <WrappedComponent />
//   </WidgetWrapper>
// }

// export { withWidget }