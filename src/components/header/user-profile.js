import styled from 'styled-components'
import React from 'react'


const ProfileWrapper = styled.div`

display:flex;
align-items: center;



.greeting{
  font-family: Inter;
font-style: normal;
font-weight: 600;
font-size: 16px;
line-height: 24px;
}

.avatar{
  width: 36px;
height: 36px;
left: 1388px;
top: 14px;
/* margin:14px; */
margin-right:14px;
margin-top:14px;
margin-left:11px;
margin-bottom:11px;
}
`

const Profile = ({ avatar, fullName }) => {
  return <ProfileWrapper>
    <div className='greeting'>Welcome {fullName}</div>
    <img className='avatar' src={avatar} alt="user ava" />
  </ProfileWrapper>
}

export { Profile }