import React from 'react'
import { CameraItem } from './camera-item'



const CameraList = ({ data }) => {
  return data.map(cam => <CameraItem name={cam.name} thumbnail={cam.thumbnail} />)
}

export { CameraList }