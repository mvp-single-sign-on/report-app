import React from 'react'
import styled from 'styled-components'
import { Chart } from 'components/chart'
import { SummaryReport } from 'components/report'


const ReportWrapper = styled.div`
margin-left:151px;
margin-right:151px;
margin-top:136px;
padding:32px;
background:red;
width:100%;
height:100%;
background:#FAFBFB;
display: flex;
flex-wrap: wrap;
`

const summaryData = [
  {
    image: '/images/sample-report/summary-report-today.svg'
  },
  {
    image: '/images/sample-report/summary-report-week.svg'
  },
  {
    image: '/images/sample-report/summary-report-month.svg'
  },
  {
    image: '/images/sample-report/map.svg'
  },
]

const data = [
  {
    country: "Minh Khai",
    station: 15,
    burgerColor: "#16C36C",
  },
  {
    country: "Hoan Kiem",
    station: 195,
    burgerColor: "#16C36C",
  },
  {
    country: "Trung Yen 3",
    station: 69,
    burgerColor: "#FF1257",
  },
  {
    country: "P.V.Dong",
    station: 103,
    burgerColor: "#F57634",
  },
  {
    country: "Nam San",
    station: 45,
    burgerColor: "#F57634",
  },
];

const SummaryReports = ({ summaryData }) => {
  return summaryData.map(report => <SummaryReport imageUrl={report.image} />)
}

const ReportContainer = () => {
  return <ReportWrapper>
    <SummaryReports summaryData={summaryData} />
    <Chart data={data} />
    <Chart data={data} />


  </ReportWrapper>
}

export { ReportContainer }