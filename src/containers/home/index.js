import React, { useState, useEffect } from 'react'
import { Header } from 'components/header'
import config from 'config'
import { CameraListContainer } from './camera-list'
import { ReportContainer } from './report'




const userInfo = {
  fullName: 'Huy Cuong',
  avatar: '/images/user-avatar/sample.svg'
}
const logo = '/images/app-icon/report-app.svg'


const Home = () => {
  const [userInfo, setUserInfo] = useState('')

  useEffect(() => {
    const fetchData = async () => {
      const userResponse = await fetch(`http://report.ilotusland.localhost:${config.serverPort}/user`, {
        credentials: 'include' // fetch won't send cookies unless you set credentials
      })
      const userInfo = await userResponse.json()
      if (userInfo.user) {
        // console.log(userInfo, '==userInfo==rese')
        setUserInfo(userInfo.user)
      } else {
        window.location.href = `http://report.ilotusland.localhost:${config.serverPort}/login`

      }

      // console.log(userInfo, '==userInfo===')
    }
    fetchData()

  }, [])
  return (
    <div>
      <Header logo={logo} avatar={userInfo.imageUrl} fullName={userInfo.email} />
      {/* <ApplicationsArea /> */}
      <ReportContainer />
    </div>
  )
}

export { Home }