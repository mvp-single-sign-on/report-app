import React from 'react'

import { Form, Input, Button, Checkbox } from 'antd';
import config from 'config'

import styled from 'styled-components'

const LoginWrapper = styled.div`
width:100%;
height:100vh;
display:flex;
justify-content:center;
align-items:center;
    /* margin-top: 9em;
    margin-right: 20em; */
    background-image:url('/images/background-login.png')
`
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const LoginPage = () => {
  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <LoginWrapper>
      <Button onClick={() => window.location.href = `http://report.ilotusland.localhost:${config.serverPort}/login`} type="primary" htmlType="submit">
        Login with iLotusland
        </Button>
    </LoginWrapper>

  );
};



export { LoginPage }